Rails.application.routes.draw do
  resources :parcellists
  resources :airinfos
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  wash_out :airwebservice
end
