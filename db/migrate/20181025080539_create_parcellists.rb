class CreateParcellists < ActiveRecord::Migration[5.2]
  def change
    create_table :parcellists do |t|
      t.integer :parcelnumber
      t.string :name
      t.string :address
      t.float :weight
      t.boolean :isDelivery

      t.timestamps
    end
  end
end
