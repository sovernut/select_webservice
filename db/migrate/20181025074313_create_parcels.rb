class CreateParcels < ActiveRecord::Migration[5.2]
  def change
    create_table :parcels do |t|
      t.integer :parcelnumber
      t.string :name
      t.string :address
      t.float :weight

      t.timestamps
    end
  end
end
