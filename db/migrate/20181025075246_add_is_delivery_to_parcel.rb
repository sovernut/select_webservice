class AddIsDeliveryToParcel < ActiveRecord::Migration[5.2]
  def change
    add_column :parcels, :isDelivery, :boolean
  end
end
