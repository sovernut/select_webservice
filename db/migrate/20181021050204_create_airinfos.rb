class CreateAirinfos < ActiveRecord::Migration[5.2]
  def change
    create_table :airinfos do |t|
      t.integer :no
      t.datetime :date_t
      t.float :temperature
      t.float :humid

      t.timestamps
    end
  end
end
