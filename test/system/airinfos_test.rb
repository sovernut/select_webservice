require "application_system_test_case"

class AirinfosTest < ApplicationSystemTestCase
  setup do
    @airinfo = airinfos(:one)
  end

  test "visiting the index" do
    visit airinfos_url
    assert_selector "h1", text: "Airinfos"
  end

  test "creating a Airinfo" do
    visit airinfos_url
    click_on "New Airinfo"

    fill_in "Date T", with: @airinfo.date_t
    fill_in "Humid", with: @airinfo.humid
    fill_in "No", with: @airinfo.no
    fill_in "Temperature", with: @airinfo.temperature
    click_on "Create Airinfo"

    assert_text "Airinfo was successfully created"
    click_on "Back"
  end

  test "updating a Airinfo" do
    visit airinfos_url
    click_on "Edit", match: :first

    fill_in "Date T", with: @airinfo.date_t
    fill_in "Humid", with: @airinfo.humid
    fill_in "No", with: @airinfo.no
    fill_in "Temperature", with: @airinfo.temperature
    click_on "Update Airinfo"

    assert_text "Airinfo was successfully updated"
    click_on "Back"
  end

  test "destroying a Airinfo" do
    visit airinfos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Airinfo was successfully destroyed"
  end
end
