require "application_system_test_case"

class ParcellistsTest < ApplicationSystemTestCase
  setup do
    @parcellist = parcellists(:one)
  end

  test "visiting the index" do
    visit parcellists_url
    assert_selector "h1", text: "Parcellists"
  end

  test "creating a Parcellist" do
    visit parcellists_url
    click_on "New Parcellist"

    fill_in "Address", with: @parcellist.address
    fill_in "Isdelivery", with: @parcellist.isDelivery
    fill_in "Name", with: @parcellist.name
    fill_in "Parcelnumber", with: @parcellist.parcelnumber
    fill_in "Weight", with: @parcellist.weight
    click_on "Create Parcellist"

    assert_text "Parcellist was successfully created"
    click_on "Back"
  end

  test "updating a Parcellist" do
    visit parcellists_url
    click_on "Edit", match: :first

    fill_in "Address", with: @parcellist.address
    fill_in "Isdelivery", with: @parcellist.isDelivery
    fill_in "Name", with: @parcellist.name
    fill_in "Parcelnumber", with: @parcellist.parcelnumber
    fill_in "Weight", with: @parcellist.weight
    click_on "Update Parcellist"

    assert_text "Parcellist was successfully updated"
    click_on "Back"
  end

  test "destroying a Parcellist" do
    visit parcellists_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Parcellist was successfully destroyed"
  end
end
