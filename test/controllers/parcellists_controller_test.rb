require 'test_helper'

class ParcellistsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @parcellist = parcellists(:one)
  end

  test "should get index" do
    get parcellists_url
    assert_response :success
  end

  test "should get new" do
    get new_parcellist_url
    assert_response :success
  end

  test "should create parcellist" do
    assert_difference('Parcellist.count') do
      post parcellists_url, params: { parcellist: { address: @parcellist.address, isDelivery: @parcellist.isDelivery, name: @parcellist.name, parcelnumber: @parcellist.parcelnumber, weight: @parcellist.weight } }
    end

    assert_redirected_to parcellist_url(Parcellist.last)
  end

  test "should show parcellist" do
    get parcellist_url(@parcellist)
    assert_response :success
  end

  test "should get edit" do
    get edit_parcellist_url(@parcellist)
    assert_response :success
  end

  test "should update parcellist" do
    patch parcellist_url(@parcellist), params: { parcellist: { address: @parcellist.address, isDelivery: @parcellist.isDelivery, name: @parcellist.name, parcelnumber: @parcellist.parcelnumber, weight: @parcellist.weight } }
    assert_redirected_to parcellist_url(@parcellist)
  end

  test "should destroy parcellist" do
    assert_difference('Parcellist.count', -1) do
      delete parcellist_url(@parcellist)
    end

    assert_redirected_to parcellists_url
  end
end
