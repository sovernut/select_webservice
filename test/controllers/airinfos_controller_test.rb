require 'test_helper'

class AirinfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @airinfo = airinfos(:one)
  end

  test "should get index" do
    get airinfos_url
    assert_response :success
  end

  test "should get new" do
    get new_airinfo_url
    assert_response :success
  end

  test "should create airinfo" do
    assert_difference('Airinfo.count') do
      post airinfos_url, params: { airinfo: { date_t: @airinfo.date_t, humid: @airinfo.humid, no: @airinfo.no, temperature: @airinfo.temperature } }
    end

    assert_redirected_to airinfo_url(Airinfo.last)
  end

  test "should show airinfo" do
    get airinfo_url(@airinfo)
    assert_response :success
  end

  test "should get edit" do
    get edit_airinfo_url(@airinfo)
    assert_response :success
  end

  test "should update airinfo" do
    patch airinfo_url(@airinfo), params: { airinfo: { date_t: @airinfo.date_t, humid: @airinfo.humid, no: @airinfo.no, temperature: @airinfo.temperature } }
    assert_redirected_to airinfo_url(@airinfo)
  end

  test "should destroy airinfo" do
    assert_difference('Airinfo.count', -1) do
      delete airinfo_url(@airinfo)
    end

    assert_redirected_to airinfos_url
  end
end
