class Airinfo < ApplicationRecord
    def self.create_new(a,b,c,d) 
        Airinfo.create(:no => a, :date_t => b, :temperature => c, :humid => d)
    end
end
class AirInfomation < WashOut::Type
    map :airinfomation => [{ :no => :integer , :date => :datetime , :temp => :double , :humid => :double}]
end

class AirInfomationWithEncrypt < WashOut::Type
    map :airinfomationEncrypt => { :iv => :string , :data => :string}
end

class StudentInfo < WashOut::Type
    map :student_information => { :stdntname => :string , :stdntnumber => :integer , :hobbys => [:string] }
end

class ParcelData < WashOut::Type
    map :parcel_information => [{ :parcelnumber => :integer , :name => :string , :address => :string , :weight => :double }]
end
