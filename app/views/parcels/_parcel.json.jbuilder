json.extract! parcel, :id, :parcelnumber, :name, :address, :weight, :created_at, :updated_at
json.url parcel_url(parcel, format: :json)
