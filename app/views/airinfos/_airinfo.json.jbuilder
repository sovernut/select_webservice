json.extract! airinfo, :id, :no, :date_t, :temperature, :humid, :created_at, :updated_at
json.url airinfo_url(airinfo, format: :json)
