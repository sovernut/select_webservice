json.extract! parcellist, :id, :parcelnumber, :name, :address, :weight, :isDelivery, :created_at, :updated_at
json.url parcellist_url(parcellist, format: :json)
