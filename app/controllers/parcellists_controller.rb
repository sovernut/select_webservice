class ParcellistsController < ApplicationController
  before_action :set_parcellist, only: [:show, :edit, :update, :destroy]

  # GET /parcellists
  # GET /parcellists.json
  def index
    @parcellists = Parcellist.all
  end

  # GET /parcellists/1
  # GET /parcellists/1.json
  def show
  end

  # GET /parcellists/new
  def new
    @parcellist = Parcellist.new
  end

  # GET /parcellists/1/edit
  def edit
  end

  # POST /parcellists
  # POST /parcellists.json
  def create
    @parcellist = Parcellist.new(parcellist_params)

    respond_to do |format|
      if @parcellist.save
        format.html { redirect_to @parcellist, notice: 'Parcellist was successfully created.' }
        format.json { render :show, status: :created, location: @parcellist }
      else
        format.html { render :new }
        format.json { render json: @parcellist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /parcellists/1
  # PATCH/PUT /parcellists/1.json
  def update
    respond_to do |format|
      if @parcellist.update(parcellist_params)
        format.html { redirect_to @parcellist, notice: 'Parcellist was successfully updated.' }
        format.json { render :show, status: :ok, location: @parcellist }
      else
        format.html { render :edit }
        format.json { render json: @parcellist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /parcellists/1
  # DELETE /parcellists/1.json
  def destroy
    @parcellist.destroy
    respond_to do |format|
      format.html { redirect_to parcellists_url, notice: 'Parcellist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_parcellist
      @parcellist = Parcellist.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def parcellist_params
      params.require(:parcellist).permit(:parcelnumber, :name, :address, :weight, :isDelivery)
    end
end
