require 'airinfo'
require 'nokogiri'
require 'openssl'
require 'base64'
require 'digest/sha1'

class AirwebserviceController < ApplicationController
    soap_service namespace: 'urn:WashOut'
    #soap_service namespace: "wash_out", wsse_username: "username", wsse_password: "password"

    soap_action "add_airinfo",
        :args   => { :no => :integer, 
                :date_t => :datetime,
                :temp => :double,
                :humid => :double },
        :return => :string
    def add_airinfo
        #now = DateTime.now()
        if params[:date_t].nil?
            raise SOAPError, "date is empty"
        else
            date_time = params[:date_t].to_time
            Airinfo.create_new(params[:no],date_time,params[:temp],params[:humid])
        end
        
        render :soap => "successfully created."
    end


    def process_array(label,array,xml)
        array.each do |hash|
          xml.send(label) do                 # Create an element named for the label
            hash.each do |key,value|
              if value.is_a?(Array)
                process_array(key,value,xml) # Recurse
              else
                xml.send(key,value)          # Create <key>value</key> (using variables)
              end
            end
          end
        end
      end
      
    def encryptText(text)
        # create the cipher for encrypting
        cipher = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
        cipher.encrypt

        # you will need to store these for later, in order to decrypt your data
        key = Digest::SHA1.hexdigest("yourpass")
        iv = cipher.random_iv

        # load them into the cipher
        cipher.key = key
        cipher.iv = iv

        # encrypt the message
        encrypted = cipher.update(Base64.encode64(text))
        encrypted << cipher.final
        return [iv,encrypted]
    end

    soap_action "get_all_airinfo",
        :args   => nil,
        :return =>  [ AirInfomationWithEncrypt ]
    def get_all_airinfo
        @posts = Airinfo.all
        array_of_post = []
        @posts.each do |post|
            hsh = {}
            hsh["no"] = post.no
            hsh["date"] = post.date_t.iso8601(0)
            hsh["temp"] = post.temperature
            hsh["humid"] = post.humid
            array_of_post.push(hsh)
        end
        builder = Nokogiri::XML::Builder.new do |xml|
            xml.root do                           # Wrap everything in one element.
              process_array('airinformation',array_of_post,xml)  # Start the recursion with a custom name.
            end
          end

        string = builder.to_xml 
        encrypttxt = encryptText(string)
        iv = encrypttxt[0]
        encrypt_text = encrypttxt[1]  
        #new_hash = { :iv => iv , :data => encrypt_text}
        
        new_hash = { :iv => "ok" , :data => encrypt_asym(string)}
        #puts iv
        puts "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
        #puts encrypt_text
        render :soap =>  [ :airinfomationEncrypt => new_hash ]
    end


    def encrypt_asym(text)
        file = File.join(Rails.root, 'app', 'keys', 'public_key2.pem')
        public_key = OpenSSL::PKey::RSA.new(File.read(file))
        encrypt = Base64.encode64(public_key.public_encrypt(text))
        return encrypt
    end

    soap_action "get_airinfo_by_no",
    :args   => { :no => :integer} ,
    :return =>  [ AirInfomation ] 
    def get_airinfo_by_no
        array_of_post = []
        Airinfo.where(no: params[:no]).find_each do |post|
            hsh = {}
            hsh["no"] = post.no
            hsh["date"] = post.date_t.iso8601(0)
            hsh["temp"] = post.temperature
            hsh["humid"] = post.humid
            array_of_post.push(hsh)
        end
        p array_of_post
        render :soap =>  [ :airinfomation=> array_of_post ]
    end

    soap_action "get_student_info",
        :args   => nil,
        :return => [StudentInfo],
        :response_tag => "student_information"
    def get_student_info
        hsh = {}
        hsh["stdntname"] = "Nuttaphon"
        hsh["stdntnumber"] = 5801012620127
        hsh["hobbys"] = ["games","movie"]
        render :soap =>  [:student_information => hsh]
    end

    soap_action "add_parcel_data",
        :args   => { :no => :integer, 
                :name => :string,
                :addr => :string,
                :weight => :double },
        :return => :string
    def add_parcel_data
        #now = DateTime.now()
        if params[:no].nil?
            raise SOAPError, "parcel number  is empty"
        else
            Parcellist.create_new(params[:no],params[:name],params[:addr],params[:weight])
        end
        
        render :soap => "successfully created parcel."
    end

    soap_action "mark_parcel_delivery",
        :args   => { :no => :integer },
        :return => :string
    def mark_parcel_delivery
        if params[:no].nil?
            raise SOAPError, "parcel number is empty"
        else
            a = Parcellist.find_by parcelnumber: params[:no]
            if a.nil?
                raise SOAPError, "not found parcel number"
            else
                p a.id
                Parcellist.update(a.id,{:isDelivery => true})
            end
        end
        
        render :soap => "successfully marked parcel."
    end

    soap_action "get_not_delivery_parcel",
    :args   => nil ,
    :return =>  [ ParcelData ] 
    def get_not_delivery_parcel
        array_of_post = []
        Parcellist.where(isDelivery: false).find_each do |post|
            hsh = {}
            hsh["parcelnumber"] = post.parcelnumber
            hsh["name"] = post.name
            hsh["address"] = post.address
            hsh["weight"] = post.weight
            array_of_post.push(hsh)
        end
        p array_of_post
        render :soap =>  [ :parcel_information=> array_of_post ]
    end
end

  
