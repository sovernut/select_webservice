class AirinfosController < ApplicationController
  before_action :set_airinfo, only: [:show, :edit, :update, :destroy]

  # GET /airinfos
  # GET /airinfos.json
  def index
    @airinfos = Airinfo.all
  end

  # GET /airinfos/1
  # GET /airinfos/1.json
  def show
  end

  # GET /airinfos/new
  def new
    @airinfo = Airinfo.new
  end

  # GET /airinfos/1/edit
  def edit
  end

  # POST /airinfos
  # POST /airinfos.json
  def create
    @airinfo = Airinfo.new(airinfo_params)

    respond_to do |format|
      if @airinfo.save
        format.html { redirect_to @airinfo, notice: 'Airinfo was successfully created.' }
        format.json { render :show, status: :created, location: @airinfo }
      else
        format.html { render :new }
        format.json { render json: @airinfo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /airinfos/1
  # PATCH/PUT /airinfos/1.json
  def update
    respond_to do |format|
      if @airinfo.update(airinfo_params)
        format.html { redirect_to @airinfo, notice: 'Airinfo was successfully updated.' }
        format.json { render :show, status: :ok, location: @airinfo }
      else
        format.html { render :edit }
        format.json { render json: @airinfo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /airinfos/1
  # DELETE /airinfos/1.json
  def destroy
    @airinfo.destroy
    respond_to do |format|
      format.html { redirect_to airinfos_url, notice: 'Airinfo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_airinfo
      @airinfo = Airinfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def airinfo_params
      params.require(:airinfo).permit(:no, :date_t, :temperature, :humid)
    end
end
